'use strict';

jQuery(document).ready(function($) {
	if( jQuery('.book-projects').length ) {

		jQuery('.book-projects .row').imagesLoaded(function() {
            jQuery('.book-projects .row').masonry({
                columnWidth: 1,
                itemSelector : 'li'
            });
        });

	}
});