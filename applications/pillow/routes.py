# -*- coding: utf-8 -*-

# routes_in = (
#     ('/cloudpillow/static/$anything', '/cloudpillow/static/$anything'),
#     ('/manager', '/cloudpillow/manager/index'),
#     ('/manager/$anything', '/cloudpillow/manager/$anything'),
#     ('/project', '/cloudpillow/project/index'),
#     ('/project/$anything', '/cloudpillow/project/$anything'),
#     ('/$anything', '/cloudpillow/default/$anything')
# )
#
# routes_out = (
#     ('/cloudpillow/manager/$anything', '/manager/$anything'),
#     ('/cloudpillow/project/$anything', '/project/$anything'),
#     ('/cloudpillow/default/$anything', '/$anything'),
#     ('/$anything', '/$anything'),
# )
