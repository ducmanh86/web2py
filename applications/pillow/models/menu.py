# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.logo = None
response.title = request.application.replace('_',' ').title()
response.subtitle = ''

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = myconf.get('app.author')
response.meta.description = myconf.get('app.description')
response.meta.keywords = myconf.get('app.keywords')
response.meta.generator = myconf.get('app.generator')

## your http://google.com/analytics id
response.google_analytics_id = None

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = [
    (T('My Site'), False, URL('default', 'index'), []),
    (T('User Manager'), False, URL('manager', 'user'), []),
    (T('Homepage Manager'), False, URL('manager', 'home'), []),
    (T('Project Manager'), False, URL('manager', 'index'), []),
    (T('Uncategory Project'), False, URL('manager', 'project'), []),
]

DEVELOPMENT_MENU = False

#########################################################################
## provide shortcuts for development. remove in production
#########################################################################

def _():
    # shortcuts
    app = request.application
    ctr = request.controller
    # useful links to internal and external resources
    response.menu += [
        (T('My Sites'), False, URL('admin', 'default', 'site')),
          (T('This App'), False, '#', [
              (T('Design'), False, URL('admin', 'default', 'design/%s' % app)),
              LI(_class="divider"),
              (T('Controller'), False,
               URL(
               'admin', 'default', 'edit/%s/controllers/%s.py' % (app, ctr))),
              (T('View'), False,
               URL(
               'admin', 'default', 'edit/%s/views/%s' % (app, response.view))),
              (T('DB Model'), False,
               URL(
               'admin', 'default', 'edit/%s/models/db.py' % app)),
              (T('Menu Model'), False,
               URL(
               'admin', 'default', 'edit/%s/models/menu.py' % app)),
              (T('Config.ini'), False,
               URL(
               'admin', 'default', 'edit/%s/private/appconfig.ini' % app)),
              (T('Layout'), False,
               URL(
               'admin', 'default', 'edit/%s/views/layout.html' % app)),
              (T('Stylesheet'), False,
               URL(
               'admin', 'default', 'edit/%s/static/css/web2py-bootstrap3.css' % app)),
              (T('Database'), False, URL(app, 'appadmin', 'index')),
              (T('Errors'), False, URL(
               'admin', 'default', 'errors/' + app)),
              (T('About'), False, URL(
               'admin', 'default', 'about/' + app)),
              ]),
          ('web2py.com', False, '#', [
             (T('Download'), False,
              'http://www.web2py.com/examples/default/download'),
             (T('Support'), False,
              'http://www.web2py.com/examples/default/support'),
             (T('Demo'), False, 'http://web2py.com/demo_admin'),
             (T('Quick Examples'), False,
              'http://web2py.com/examples/default/examples'),
             (T('FAQ'), False, 'http://web2py.com/AlterEgo'),
             (T('Videos'), False,
              'http://www.web2py.com/examples/default/videos/'),
             (T('Free Applications'),
              False, 'http://web2py.com/appliances'),
             (T('Plugins'), False, 'http://web2py.com/plugins'),
             (T('Recipes'), False, 'http://web2pyslices.com/'),
             ]),
          (T('Documentation'), False, '#', [
             (T('Online book'), False, 'http://www.web2py.com/book'),
             LI(_class="divider"),
             (T('Preface'), False,
              'http://www.web2py.com/book/default/chapter/00'),
             (T('Introduction'), False,
              'http://www.web2py.com/book/default/chapter/01'),
             (T('Python'), False,
              'http://www.web2py.com/book/default/chapter/02'),
             (T('Overview'), False,
              'http://www.web2py.com/book/default/chapter/03'),
             (T('The Core'), False,
              'http://www.web2py.com/book/default/chapter/04'),
             (T('The Views'), False,
              'http://www.web2py.com/book/default/chapter/05'),
             (T('Database'), False,
              'http://www.web2py.com/book/default/chapter/06'),
             (T('Forms and Validators'), False,
              'http://www.web2py.com/book/default/chapter/07'),
             (T('Email and SMS'), False,
              'http://www.web2py.com/book/default/chapter/08'),
             (T('Access Control'), False,
              'http://www.web2py.com/book/default/chapter/09'),
             (T('Services'), False,
              'http://www.web2py.com/book/default/chapter/10'),
             (T('Ajax Recipes'), False,
              'http://www.web2py.com/book/default/chapter/11'),
             (T('Components and Plugins'), False,
              'http://www.web2py.com/book/default/chapter/12'),
             (T('Deployment Recipes'), False,
              'http://www.web2py.com/book/default/chapter/13'),
             (T('Other Recipes'), False,
              'http://www.web2py.com/book/default/chapter/14'),
             (T('Helping web2py'), False,
              'http://www.web2py.com/book/default/chapter/15'),
             (T("Buy web2py's book"), False,
              'http://stores.lulu.com/web2py'),
             ]),
          (T('Community'), False, None, [
             (T('Groups'), False,
              'http://www.web2py.com/examples/default/usergroups'),
              (T('Twitter'), False, 'http://twitter.com/web2py'),
              (T('Live Chat'), False,
               'http://webchat.freenode.net/?channels=web2py'),
              ]),
        ]
if DEVELOPMENT_MENU: _()

if "auth" in locals(): auth.wikimenu() 


def _get_menu():
    def render_children(parent):
        _pane = DIV(_class="ui-accordion-content")
        _menu = DIV(_class='accordion ui-accordion ui-widget ui-helper-reset')
        children = db(db.category.parent == parent).select(db.category.id, db.category.name,
                                                           orderby=db.category.cat_order)
        for child in children:
            if request.function == 'catalog' and request.args[0] == str(child.id):
                _menu.append(H3(SPAN(_class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"),
                             A(child.name, _href=URL(c='project', f='catalog', args=child.id), _class='selected'),
                             _class='ui-accordion-header'))
            else:
                _menu.append(H3(SPAN(_class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"),
                             A(child.name, _href=URL(c='project', f='catalog', args=child.id)),
                             _class='ui-accordion-header'))
            _menu.append(render_children(child['id']))
        if len(children) > 0:
            _pane.append(_menu)
        _pjs = db(db.project.category == parent).select(db.project.id, db.project.title)
        for _pj in _pjs:
            if request.controller == 'project' and request.function == 'index' and request.args[0] == str(_pj.id):
                _pane.append(LI(A(_pj.title, _href=URL(c='project', f='index', args=_pj.id), _class='selected')))
            else:
                _pane.append(LI(A(_pj.title, _href=URL(c='project', f='index', args=_pj.id))))
        return _pane

    menu = DIV(_class='accordion ui-accordion ui-widget ui-helper-reset')
    cats = db(db.category.parent == None).select(db.category.id, db.category.name)
    for cat in cats:
        if request.function == 'catalog' and request.args[0] == str(cat.id):
            menu.append(H3(SPAN(_class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"),
                        A(cat.name, _href=URL(c='project', f='catalog', args=cat.id), _class='selected'),
                        _class='ui-accordion-header'))
        else:
            menu.append(H3(SPAN(_class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"),
                        A(cat.name, _href=URL(c='project', f='catalog', args=cat.id)),
                        _class='ui-accordion-header'))
        menu.append(render_children(cat.id))
    pjs = db(db.project.category == None).select(db.project.id, db.project.title)
    for pj in pjs:
        if request.controller == 'project' and request.function == 'index' and request.args[0] == str(pj.id):
            menu.append(LI(A(pj.title, _href=URL(c='project', f='index', args=pj.id), _class='selected')))
        else:
            menu.append(LI(A(pj.title, _href=URL(c='project', f='index', args=pj.id))))
    return menu


def _get_project_list():
    menu = []
    projs = db(db.project.id > 0).select(db.project.id, db.project.title)
    for p in projs:
        menu.append(LI(A(p.title, _href=URL(c='project', f='index', args=p.id))))
    return menu

if request.function != 'download':
    response.categories = _get_menu()
