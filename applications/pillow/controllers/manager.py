# -*- coding: utf-8 -*-
__author__ = 'manhtd'


@auth.requires_login()
def project():
    if 'edit' not in request.args:
        db.project.category.readable = False
    query = db.project.category == None
    constraints = {'project': query}
    grid = SQLFORM.smartgrid(db.project, csv=False, showbuttontext=False, maxtextlength=100,
                             paginate=5, constraints=constraints)
    return dict(grid=grid)


@auth.requires_login()
def home():
    grid = SQLFORM.smartgrid(db.home, csv=False, showbuttontext=False, maxtextlength=100)
    return dict(grid=grid)


@auth.requires_login()
def index():
    # def get_all_childrent(root):
    #     """
    #     get all children of category
    #     @param root:
    #     @return:
    #     """
    #     leafs = []
    #
    #     def _get_all_childrent(node):
    #         leafs.append(node)
    #         children = db(db.category.parent == node).select(db.category.id)
    #         for child in children:
    #             _get_all_childrent(child.id)
    #
    #     _get_all_childrent(root)
    #     return leafs

    orderby = "cat_order"
    if 'image.project' in request.args:
        orderby = ''
    elif 'project.category' in request.args:
        orderby = "project_order"
    elif 'category.parent' in request.args:
        db.category.parent.represent = lambda x: '' if x is None else db(db.category.id == x).select(
            db.category.name).first().name
    else:
        db.category.parent.represent = lambda x, row: '' if x is None else db(db.category.id == x).select(
            db.category.name).first().name

    if 'edit' not in request.args and 'view' not in request.args:
        db.project.description.readable = False
    elif 'view' in request.args:
        db.project.description.represent = lambda value: XML(value)

    constraints = {}
    if len(request.args) < 2:
        db.category.parent.readable = False
        query = db.category.parent == None
        constraints = {'category': query}
    # elif request.args[-2] == 'project.category':
        # request.args[-1] = '2,1'
        # cid = int(request.args[-1])
        # nodes = get_all_childrent(cid)
        # query = db.project.category > 0
        # constraints = {'project': query}

    grid = SQLFORM.smartgrid(db.category, csv=False, showbuttontext=False, maxtextlength=100,
                             orderby=orderby, constraints=constraints, paginate=5)
    return dict(grid=grid)


# @auth.requires_login()
# def project():
#     orderby = "project_order"
#     if 'image.project' in request.args:
#         orderby = ''
#     grid = SQLFORM.smartgrid(db.project, csv=False, showbuttontext=False, maxtextlength=100, orderby=orderby)
#     return dict(grid=grid)

@auth.requires_login()
def user():
    auth_user = auth.table_user()
    auth_user.id.readable = False
    query = auth_user.id != auth.user_id
    constraints = {str(auth_user): query}
    kwargs = dict(user_signature=True, maxtextlength=1000, linked_tables=[], constraints=constraints)
    grid = SQLFORM.smartgrid(auth.table_user(), csv=False, showbuttontext=False, **kwargs)
    return dict(grid=grid)
