# -*- coding: utf-8 -*-
__author__ = 'manhtd'


def index():
    try:
        if len(request.args) != 1:
            redirect(URL('default', 'index'))
        int(request.args[0])
    except ValueError:
        redirect(URL('default', 'index'))
    project = db(db.project.id == request.args[0]).select(db.project.title, db.project.description)
    if len(project) == 0:
        redirect(URL('default', 'index'))
    images = db(db.image.project == request.args[0]).select(db.image.image, db.image.alt_tag, db.image.title)
    return dict(project=project.first(), images=images)


def catalog():
    def get_children(parent):
        _pjs = db(db.project.category == parent).select(db.project.id, db.project.title, db.project.thumbnail).as_list()
        _children = db(db.category.parent == parent).select(db.category.id, orderby=db.category.cat_order)
        for child in _children:
            _pjs.extend(get_children(child.id))
        return _pjs

    try:
        if len(request.args) != 1:
            redirect(URL('default', 'index'))
        int(request.args[0])
    except ValueError:
        redirect(URL('default', 'index'))

    projs = get_children(request.args[0])
    if len(projs) == 1:
        redirect(URL('index', args=projs.first().id))
    return dict(projects=projs)


def detail():
    return  dict()