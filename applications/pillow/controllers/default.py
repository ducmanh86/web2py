# -*- coding: utf-8 -*-
def index():
    result = []
    home = db(db.home.id > 0).select(db.home.project, db.home.category, orderby=db.home.cat_order)
    for item in home:
        if item.category is not None:
            cat = db(db.category.id == item.category).select(db.category.name, db.category.thumbnail).first()
            result.append(dict(url=URL(c='project', f='catalog', args=item.category),
                               title=cat.name, thumbnail=cat.thumbnail))
        elif item.project is not None:
            pjs = db(db.project.id == item.project).select(db.project.title, db.project.thumbnail).first()
            result.append(dict(url=URL(c='project', f='index', args=item.project),
                               title=pjs.title, thumbnail=pjs.thumbnail))
    return dict(home=result)


def about():
    return dict()


def contact():
    import random
    if len(request.vars) == 4 and all(name in request.vars for name in ['key', 'name', 'email', 'message']):
        if request.vars.key == str(session.form_key):
            message = "<html><body>Name: %s<br>Email: %s<br>Message: %s</body></html>" % (request.vars.name,
                                                                                          request.vars.email,
                                                                                          request.vars.message)
            mail.send("cloudpillowstudio@gmail.com", subject="Mail Contact From CloudPillow.net", message=message)
            session.form_key = random.randrange(1000, 9999)
            return dict(alert=True)
    session.form_key = random.randrange(1000, 9999)
    return dict(alert=False)


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


