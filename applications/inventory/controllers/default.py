# -*- coding: utf-8 -*-


def index():
    redirect(URL('detail'))


@auth.requires_login()
def detail():
    form = SQLFORM.smartgrid(db.good_detail, csv=False, linked_tables=[])
    return locals()


@auth.requires_login()
def category():
    form = SQLFORM.smartgrid(db.good_category, csv=False)
    return locals()


@auth.requires_login()
def gtype():
    form = SQLFORM.smartgrid(db.good_type, csv=False)
    return locals()


@auth.requires_login()
def color():
    form = SQLFORM.smartgrid(db.good_color, csv=False)
    return locals()


@auth.requires_login()
def size():
    form = SQLFORM.smartgrid(db.good_size, csv=False)
    return locals()


@auth.requires_login()
def supplier():
    form = SQLFORM.smartgrid(db.good_supplier, csv=False)
    return locals()


@auth.requires_login()
def history():
    form = SQLFORM.smartgrid(db.good_export, csv=False, editable=False, deletable=False, create=False)
    return locals()


@auth.requires_login()
def sell():
    form = FORM(DIV(LABEL('Good', _class='control-label col-sm-3'),
                    DIV(SELECT(OPTION('Chọn loại hàng hóa'), _name='good', _class='generic-widget form-control'),
                        _class='col-sm-9'), _class='form-group'),
                DIV(LABEL('Amount', _class='control-label col-sm-3'),
                    DIV(INPUT(_name='good', _class='form-control integer'), _class='col-sm-9'), _class='form-group'),
                DIV(DIV(INPUT(_type='submit', _class='btn btn-primary'), _class='col-sm-9 col-sm-offset-3'),
                    _class='form-group'),
                _method='POST', _class='form-horizontal web2py_form')
    return dict(form=form)


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
